package com.ssafy.algo;

import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;

public class Solution13_채희준 {
	public static final int[] dx = { -1, -1, 0, 1, 1, 1, 0, -1 };
	public static final int[] dy = { 0, 1, 1, 1, 0, -1, -1, -1 };// 이게 편한거 같음 (1)
	// 8개 우측순회. 가능 8번돌려면 8개 우측순회.

	public static void main(String[] args) throws Exception {

		System.setIn(new FileInputStream("res/input1.txt"));
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();

		for (int tc = 0; tc < T; tc++) {

			int N = sc.nextInt();
			char[][] arr = new char[N][N];
			boolean[][] check = new boolean[N][N];
			for (int i = 0; i < N; i++) {// 캐릭터 2차원 배열 , string 하나, string을 to char(0)인덱스로
				for (int j = 0; j < N; j++) {// 치환한거를 [i][j]에 입력.
					String line = sc.next();
					arr[i][j] = line.charAt(0);
				}
//				System.out.println(Arrays.deepToString(arr));// deeptostring으로 배열출력값 확인.
			}

			int max = 1;// 전부다 G일경우 0 출력
			int sum = 0;
//			char checker = 'G';
			for (int i = 0; i < N; i++) {// 캐릭터 2차원 배열 , string 하나, string을 to char(0)인덱스로
				for (int j = 0; j < N; j++) {// 치환한거를 [i][j]에 입력.
					sum = 0;
					if (arr[i][j] == 'G') {
						continue;
					}
					if (max == 1)
						max = 3; // 전부다 G에 하나만 B일때는 2출력 (G가있을 땐 2를 출력)

					for (int d = 0; d < dx.length; d++) {
						int nx = i + dx[d];
						int ny = j + dy[d];

						// 좌표값 넘어가면 continue
						if (0 <= nx && nx < N && 0 <= ny && ny < N) {
							// checker를 만나도 continue! 근데 에러 ?
							if (arr[nx][ny] == 'G') {
								check[i][j] = false;
								break;
							}
							check[i][j] = true;
						}
					}

					if (check[i][j]) {
						for (int k = 0; k < N; k++) {
							if (arr[i][k] == 'B')
								sum += 1;
						}
						for (int k = 0; k < N; k++) {
							if (arr[k][j] == 'B')
								sum += 1;
						}
					}
					if (sum > max)
						max = sum;
				}
			}
			System.out.println("#" + (tc + 1) + " " + (max - 1));
		}

	}

}

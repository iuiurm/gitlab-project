# -*- coding: utf-8 -*-
import re
import urllib.request

from bs4 import BeautifulSoup
from bs4 import Comment

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter
from slack.web.classes import extract_json
from slack.web.classes.blocks import *

# OAuth & Permissions로 들어가서
# Bot User OAuth Access Token을 복사하여 문자열로 붙여넣습니다
SLACK_TOKEN = "xoxb-691766829590-689159042132-dVNAL2HF2SCdExOn6Mp6Ty9t"
# Basic Information으로 들어가서
# Signing Secret 옆의 Show를 클릭한 다음, 복사하여 문자열로 붙여넣습니다
SLACK_SIGNING_SECRET = "2949d12313571ae9dd112c7da42da001"

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)


#**********************전역변수***************
imglist = ['https://imagescdn.gettyimagesbank.com/500/14/211/072/3/a8945142.jpg',
           'https://imagescdn.gettyimagesbank.com/500/14/211/081/3/a8945133.jpg',
           'https://imagescdn.gettyimagesbank.com/500/14/211/075/3/a8945139.jpg',
           'https://imagescdn.gettyimagesbank.com/500/14/211/073/3/a8945141.jpg',
           'https://imagescdn.gettyimagesbank.com/500/14/211/076/3/a8945138.jpg',
           'https://imagescdn.gettyimagesbank.com/500/14/211/082/3/a8945132.jpg',
           'https://imagescdn.gettyimagesbank.com/500/14/211/077/3/a8945137.jpg',
           'https://imagescdn.gettyimagesbank.com/500/14/211/080/3/a8945134.jpg',
           'https://imagescdn.gettyimagesbank.com/500/14/211/079/3/a8945135.jpg',
           'https://imagescdn.gettyimagesbank.com/500/14/211/074/3/a8945140.jpg',
           'https://imagescdn.gettyimagesbank.com/500/14/211/071/3/a8945143.jpg',
           'https://imagescdn.gettyimagesbank.com/500/14/211/078/3/a8945136.jpg',
           '36601.png', '36601.png'
           ]


# 도움말 함수
def _helpbot(text):
    help = [
        "안녕하세요! 스텔라봇의 주요 기능을 알려드리겠습니다.",
        "*이번 달 별자리운세*    `@stella 이번달 황소자리` 와 같이 입력해주세요",
        "*이번 주 별자리운세*    `@stella 이번주 황소자리` 와 같이 입력해주세요",
        "*이번 주 별자리 금전운* `@stella 이번주 황소자리 금전운` 과 같이 입력해주세요",
        "*오늘의 별자리운세*     `@stella 오늘의 황소자리` 와 같이 입력해주세요",
        "*나의 별자리 찾기*      `@stella 별자리 알려줘` 와 같이 입력해주세요"
    ]
    return u'\n'.join(help)

#오늘의 별자리
def _todaycrawl(text):
    for i in range(1, 2):

        url = "http://sazoo.com/ss/run/tarot/month_free/index.php?idx=" + str(i)
        url2 = "http://sazoo.com/ss/run/tarot/star_today/"

        req = urllib.request.Request(url)
        req2 = urllib.request.Request(url2)

        sourcecode = urllib.request.urlopen(url).read()
        sourcecode2 = urllib.request.urlopen(url2).read()  # 2는 오늘의 운세

        soup = BeautifulSoup(sourcecode, "html.parser")
        soup2 = BeautifulSoup(sourcecode2, "html.parser")

        stars = ['양', '황소', "쌍둥이", "게", "사자", "처녀", "천칭", "전갈", "사수", "염소", "물병", "물고기"]

        starnum = 13  # 무슨 자리인가

        keywords = ["양자리, 황소자리, 쌍둥이자리, 게자리, 사자자리, 처녀자리, 천칭자리, 전갈자리, 사수자리, 염소자리, 물병자리, 물고기 자리중에 골라주세요."]
        for num in range(len(stars)):
            # 무슨 자리인지 확인
            if stars[num] in text:
                starnum = num
                keywords = ["*" + stars[starnum] + "자리 별자리 운세*"]

        if starnum == 13:
            return (u'\n'.join(keywords), starnum)

        message = []
        msg1=[]
        msg2=[]

        final_daily=[]
        final_dlove=[]
        daily_final=[]

        what = [1, 2, 3, 4]
        # print(soup2)
        # list=[ 44,50,56,62,68,74,78,84 ]
        # for i in len(list):
        for meta in soup2.find_all('td', width="300"):


            message.append(str(meta.get_text()))


        for j in message:
            msg1=j.split()

            msg2=msg1[3:(len(msg1)-3)]
            msg3=''
            msg4=''
            flag=False
            for i in msg2:

                if "애정운" in i:
                    temp = i.split("애정운") # 애정운을 기준으로 앞뒤를 자름 , 애정운은 없어진다.
                    flag=True
                    msg3 += temp[0]
                    msg4+= temp[1]
                elif flag:
                    msg4 += i+ " "
                else:
                    msg3 += i+" "

            final_daily.append(msg3)
            final_dlove.append(msg4)

        for i in range(len(final_daily)):
            daily_final.append ("`오늘의 총평` "+final_daily[i] + " \n\n `애정운` " + final_dlove[i])



    return (u''.join("*" + stars[starnum] + "자리 별자리 운세*\n"+ daily_final[starnum]), starnum)


def _monthcrawl(text):
    starnum = 13
    stars = ['양', '황소', "쌍둥이", "게", "사자", "처녀", "천칭", "전갈", "사수", "염소", "물병", "물고기"]

    for num in range(len(stars)):
        # 무슨 자리인지 확인
        if stars[num] in text:
            starnum = num

    if starnum == 13:
        keywords = ["양자리, 황소자리, 쌍둥이자리, 게자리, 사자자리, 처녀자리, 천칭자리, 전갈자리, 사수자리, 염소자리, 물병자리, 물고기 자리중에 골라주세요."]
        return u'\n'.join(keywords, starnum)


    url = "http://sazoo.com/ss/run/tarot/month_free/index.php?idx=" + str(starnum+1)
    req = urllib.request.Request(url)
    sourcecode = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(sourcecode, "html.parser")



    message = []
    msg2 = []
    msg3 = []

    for meta in soup.find_all('td', width="545"):

        # print(st1)
        message.append(meta.get_text())
        msg2 = message[0].split("\r\n")

        # print(msg2)
        st1 = ''
        for i in range(0, 8):
            if i % 2 == 0:
                continue
            else:
                st1 = st1 + msg2[i] + "\n"

        msg3 = st1.split("\n")

    msg3[0] = "`이번 달 총평` " + msg3[0] +"\n"
    msg3[1] = "`싱글 연애운` " + msg3[1] +"\n"
    msg3[2] = "`커플 연애운` " + msg3[2] +"\n"
    msg3[3] = "`이번 달 금전운` " + msg3[3] +"\n"

    msg2 = ["*" + stars[starnum] + "자리 별자리 월간 운세*"]
    for i in range(4):
        msg2.append(msg3[i])

    return (u'\n'.join(msg2), starnum)


#별자리 이번주운세 크롤링
def _weekcrawl(text):
    stars = ["양", "황소", "쌍둥이", "게", "사자", "처녀", "천칭", "전갈", "사수", "염소", "물병", "물고기"]
    starnum = 13     #무슨 자리인가

    keywords = ["양자리, 황소자리, 쌍둥이자리, 게자리, 사자자리, 처녀자리, 천칭자리, 전갈자리, 사수자리, 염소자리, 물병자리, 물고기 자리중에 골라주세요."]
    for num in range(len(stars)):
        #무슨 자리인지 확인
        if stars[num] in text:
            starnum = num
            keywords = ["*" + stars[starnum] + "자리 별자리 운세*"]

    if starnum == 13:
        return (u'\n'.join(keywords), starnum)

  #******************************************************금전운*******************************
    if "금전운" in text:
        moneyurls = ["http://sazoo.com/ss/run/tarot/star_free/index.php?idx=%BE%E7%C0%DA%B8%AE",
                     "http://sazoo.com/ss/run/tarot/star_free/index.php?idx=%C8%B2%BC%D2%C0%DA%B8%AE",
                     "http://sazoo.com/ss/run/tarot/star_free/index.php?idx=%BD%D6%B5%D5%C0%CC%C0%DA%B8%AE",
                     "http://sazoo.com/ss/run/tarot/star_free/index.php?idx=%B0%D4%C0%DA%B8%AE",
                     "http://sazoo.com/ss/run/tarot/star_free/index.php?idx=%BB%E7%C0%DA%C0%DA%B8%AE",
                     "http://sazoo.com/ss/run/tarot/star_free/index.php?idx=%C3%B3%B3%E0%C0%DA%B8%AE",
                     "http://sazoo.com/ss/run/tarot/star_free/index.php?idx=%C3%B5%C4%AA%C0%DA%B8%AE",
                     "http://sazoo.com/ss/run/tarot/star_free/index.php?idx=%C0%FC%B0%A5%C0%DA%B8%AE",
                     "http://sazoo.com/ss/run/tarot/star_free/index.php?idx=%BB%E7%BC%F6%C0%DA%B8%AE",
                     "http://sazoo.com/ss/run/tarot/star_free/index.php?idx=%BF%B0%BC%D2%C0%DA%B8%AE",
                     "http://sazoo.com/ss/run/tarot/star_free/index.php?idx=%B9%B0%BA%B4%C0%DA%B8%AE",
                     "http://sazoo.com/ss/run/tarot/star_free/index.php?idx=%B9%B0%B0%ED%B1%E2%C0%DA%B8%AE",
                     ]

        url = moneyurls[starnum]
        #print("\n" + stars[starnum], "자리", end='  ')

        source_code = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(source_code, "html.parser")
        howmuch = True
        for abc in soup.find_all("td"):
            for comments in abc.find_all(text=lambda text: isinstance(text, Comment)):
                if "금전운" in comments and not "//" in comments:
                    if len(abc.get_text().replace("\n", "")) < 200 and howmuch:
                        #howmuch = False
                        keywords.append("`이번주 금전운` " + abc.get_text().replace("\n", ""))

        return (u'\n'.join(keywords), starnum)

    # ************************************별자리 총평***********************

    urls = ["http://sazoo.com/ss/run/tarot/star_free/index.php?idx=%BE%E7%C0%DA%B8%AE",
            "http://sazoo.com/ss/run/tarot/star_free/index.php?idx=%C8%B2%BC%D2%C0%DA%B8%AE",
            "http://sazoo.com/ss/run/tarot/star_free/index.php?idx=%BD%D6%B5%D5%C0%CC%C0%DA%B8%AE",
            "http://sazoo.com/ss/run/tarot/star_free/index.php?idx=%B0%D4%C0%DA%B8%AE",
            "http://sazoo.com/ss/run/tarot/star_free/index.php?idx=%BB%E7%C0%DA%C0%DA%B8%AE",
            "http://sazoo.com/ss/run/tarot/star_free/index.php?idx=%C3%B3%B3%E0%C0%DA%B8%AE",
            "http://sazoo.com/ss/run/tarot/star_free/index.php?idx=%C3%B5%C4%AA%C0%DA%B8%AE",
            "http://sazoo.com/ss/run/tarot/star_free/index.php?idx=%C0%FC%B0%A5%C0%DA%B8%AE",
            "http://sazoo.com/ss/run/tarot/star_free/index.php?idx=%BB%E7%BC%F6%C0%DA%B8%AE",
            "http://sazoo.com/ss/run/tarot/star_free/index.php?idx=%BF%B0%BC%D2%C0%DA%B8%AE",
            "http://sazoo.com/ss/run/tarot/star_free/index.php?idx=%B9%B0%BA%B4%C0%DA%B8%AE",
            "http://sazoo.com/ss/run/tarot/star_free/index.php?idx=%B9%B0%B0%ED%B1%E2%C0%DA%B8%AE",
            ]
    url = urls[starnum]

    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    for abc in soup.find_all("td"):
        if (abc.get_text().startswith('이번')):  # 전체운
            keywords.append("`이번주 총평` " + abc.get_text())

    return (u'\n'.join(keywords), starnum)

#별자리 판단 요건
def _whatis(text):
    rtlist = []
    p = re.compile('[0-9]+')
    m = p.findall(text)

    month = int(m[2])
    day = int(m[3])

    days = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    #print(month, day)
    if not 0 < month < 13:
        return "유효하지 않은 날짜입니다. 다시 입력해주세요\n"
    if not 0 < day <= days[month - 1]:
        return "유효하지 않은 날짜입니다. 다시 입력해주세요\n"
    #날짜 바꾸기

    x = (month *100) + day
    print(x)

    if 120 <= x <= 218:
        rtlist = ["당신의 별자리는 *물병자리*입니다."]
    elif 219 <= x <= 320:
        rtlist = [ '당신의 별자리는 *물고기자리*입니다.']
    elif 321 <= x <= 420:
        rtlist = [ '당신의 별자리는 *양자리*입니다.']
    elif 421 <= x <= 520:
        rtlist = [ '당신의 별자리는 *황소자리*입니다.']
    elif 521 <= x <= 621:
        rtlist = [ '당신의 별자리는 *쌍둥이자리*입니다.']
    elif 622 <= x <= 722:
        rtlist = [ '당신의 별자리는 *게자리*입니다.']
    elif 723 <= x <= 822:
        rtlist = [ '당신의 별자리는 *사자자리*입니다.']
    elif 823 <= x <= 922:
        rtlist = [ '당신의 별자리는 *처녀자리*입니다.']
    elif 923 <= x <= 1021:
        rtlist = [ '당신의 별자리는 *천칭자리*입니다.']
    elif 1022 <= x <= 1121:
        rtlist = [ '당신의 별자리는 *전갈자리*입니다.']
    elif 1122 <= x <= 1221:
        rtlist = [ '당신의 별자리는 *사수자리*입니다.']
    else:
        rtlist = [ '당신의 별자리는 *염소자리*입니다.']

    return u'\n'.join(rtlist)

# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    print(text)


    if ("별자리" in text and "알려줘" in text):
        message = "생일을 입력해주세요.\n예: 5월 22일"
        slack_web_client.chat_postMessage(
            channel=channel,
            text=message
        )
    elif ("별자리" in text):
            message = "안녕하세요! 당신의 별자리를 말해주세요!\n별자리를 모르신다면 *별자리 알려줘* 라고 입력해주세요."
            slack_web_client.chat_postMessage(
                channel=channel,
                text=message
            )
    elif ("오늘" in text and "자리" in text):
        message, starnum = _todaycrawl(text)
        # 이미지 블록: 큰 이미지 하나를 표시합니다..
        block1 = ImageBlock(
            image_url=imglist[starnum],
            alt_text="별자리"
        )
        block2 = SectionBlock(
            text=message
        )
        # 여러 개의 블록을 하나의 메시지로 묶어 보냅니다.
        my_blocks = [block1, block2]
        slack_web_client.chat_postMessage(
            channel=channel,
            blocks=extract_json(my_blocks)
        )

    elif (("주"in text) and "자리" in text):
        message, starnum = _weekcrawl(text)
        # 이미지 블록: 큰 이미지 하나를 표시합니다..
        block1 = ImageBlock(
            image_url=imglist[starnum],
            alt_text="별자리"
        )
        block2 = SectionBlock(
            text=message
        )
        # 여러 개의 블록을 하나의 메시지로 묶어 보냅니다.
        my_blocks = [block1, block2]
        slack_web_client.chat_postMessage(
            channel=channel,
            blocks=extract_json(my_blocks)
        )

    elif (("월" in text or "달" in text) and "자리" in text):
        message, starnum = _monthcrawl(text)

        # 이미지 블록: 큰 이미지 하나를 표시합니다..
        block1 = ImageBlock(
            image_url=imglist[starnum],
            alt_text="별자리"
        )
        block2 = SectionBlock(
            text=message
        )
        # 여러 개의 블록을 하나의 메시지로 묶어 보냅니다.
        my_blocks = [block1, block2]
        slack_web_client.chat_postMessage(
            channel=channel,
            blocks=extract_json(my_blocks)
        )

        # slack_web_client.chat_postMessage(
        #     channel=channel,
        #     text=message
        # )

    #이번주 금전운
    elif ("금전운"in text and "자리" in text):
        message, starnum = _weekcrawl(text)
        # 이미지 블록: 큰 이미지 하나를 표시합니다..
        block1 = ImageBlock(
            image_url=imglist[starnum],
            alt_text="별자리"
        )
        block2 = SectionBlock(
            text=message
        )
        # 여러 개의 블록을 하나의 메시지로 묶어 보냅니다.
        my_blocks = [block1, block2]
        slack_web_client.chat_postMessage(
            channel=channel,
            blocks=extract_json(my_blocks)
        )

    elif ("월" in text and "일" in text):
        message = _whatis(text)
        slack_web_client.chat_postMessage(
            channel=channel,
            text=message
        )

    #디폴트는 오늘의 운세
    elif "자리" in text:
        message, starnum  = _todaycrawl(text)

        # 이미지 블록: 큰 이미지 하나를 표시합니다..
        block1 = ImageBlock(
            image_url=imglist[starnum],
            alt_text="별자리"
        )
        block2 = SectionBlock(
            text=message
        )
        # 여러 개의 블록을 하나의 메시지로 묶어 보냅니다.
        my_blocks = [block1, block2]
        slack_web_client.chat_postMessage(
            channel=channel,
            blocks=extract_json(my_blocks)
        )

    #설명
    else:
        message = _helpbot(text)
        slack_web_client.chat_postMessage(
            channel=channel,
            text=message
        )


# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('127.0.0.1', port=8000)
